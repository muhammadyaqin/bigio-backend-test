<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class SiswaController extends Controller
{
    public function ViewSiswa (Request $request) {
        $page = 1;
        $next = $page+1;
        $prev = $page-1;
        $numbering = 0;
        if ($request->page) {
            $page = $request->page;
            $numbering = $page*10;
        }
        if ($request->page == 1) {
            $numbering = $page-1;
        }
        $siswa = DB::table('users')
        ->select('users.id','users.name','users.email','roles.name as nama_posisi')
        ->leftjoin('model_has_roles as posisi', 'users.id','posisi.model_id')
        ->leftjoin('roles','posisi.role_id','roles.id')
        ->where('posisi.role_id', 3)
        ->paginate(10);
        $data['siswa'] = $siswa;
        return view('siswa.view', $data, compact('page', 'next', 'prev','numbering'));
    }

    public function TambahSiswa(Request $request){
        return view('siswa.tambah');
    }

    public function EditSiswa($id){
        $siswa = DB::table('users')->where('id', $id)->first();
        $data['siswa'] = $siswa;
        return view('siswa.edit', $data);
    }

    public function InsertSiswa(Request $request){
        $name = $request->name;
        $email = $request->email;
        $created_at = Carbon::now();

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($request->password),
            'created_at' => $created_at,
        ]);

        $role = Role::findByName('siswa');
        $user->assignRole($role);

        return redirect()->route('view_siswa')->with('success', 'Berhasil');;
    }

    public function UpdateSiswa(Request $request){
        $id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $update_at = Carbon::now();

        if($request->password != null) {
            User::where('id', $id)->update([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($request->password),
                'updated_at' => $update_at,
            ]);
        }
        else {
            User::where('id', $id)->update([
                'name' => $name,
                'email' => $email,
                'updated_at' => $update_at,
            ]);
        }

        return redirect()->route('view_siswa')->with('success', 'Berhasil');;
    }

    public function DeleteSiswa($id){
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();

        return redirect()->route('view_siswa')->with('success', 'Berhasil');
    }
}
