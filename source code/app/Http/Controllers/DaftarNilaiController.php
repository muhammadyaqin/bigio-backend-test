<?php

namespace App\Http\Controllers;
use DB;
use Carbon\Carbon;
use Auth;

use Illuminate\Http\Request;

class DaftarNilaiController extends Controller
{
    Public function ViewNilai(Request $request){
        $page = 1;
        $next = $page+1;
        $prev = $page-1;
        $numbering = 0;
        if ($request->page) {
            $page = $request->page;
            $numbering = $page*10;
        }
        if ($request->page == 1) {
            $numbering = $page-1;
        }
        $nilai = DB::table('nilai')
        ->select('siswa.name as nama_siswa','matpel.nama_matpel', 'nilai.nilai', 'nilai.nama_inputer', 'nilai.id')
        ->leftjoin('users as siswa', 'nilai.id_siswa', 'siswa.id')
        ->leftjoin('matpel', 'nilai.id_matpel', 'matpel.id')
        ->where('nilai.nama_inputer', Auth::user()->name)
        ->paginate(10);
        $data['nilai'] = $nilai;
        return view('nilai.view', $data, compact('page', 'next', 'prev','numbering'));
    }

    Public function LihatNilai(Request $request){
        $page = 1;
        $next = $page+1;
        $prev = $page-1;
        $numbering = 0;
        if ($request->page) {
            $page = $request->page;
            $numbering = $page*10;
        }
        if ($request->page == 1) {
            $numbering = $page-1;
        }

        $check_role = DB::table('users')
        ->select('users.name', 'role.role_id', 'role.model_id')
        ->leftjoin('model_has_roles as role', 'users.id', 'role.model_id')
        ->where('users.name', Auth::user()->name)->first();

        if($check_role->role_id == 3){
            $nilai = DB::table('nilai')
            ->select('siswa.name as nama_siswa','matpel.nama_matpel', 'nilai.nilai', 'nilai.id')
            ->leftjoin('users as siswa', 'nilai.id_siswa', 'siswa.id')
            ->leftjoin('matpel', 'nilai.id_matpel', 'matpel.id')
            ->where('nilai.id_siswa', Auth::user()->id)
            ->paginate(10);
        }
        else{
            $nilai = DB::table('nilai')
            ->select('siswa.name as nama_siswa','matpel.nama_matpel', 'nilai.nilai', 'nilai.id')
            ->leftjoin('users as siswa', 'nilai.id_siswa', 'siswa.id')
            ->leftjoin('matpel', 'nilai.id_matpel', 'matpel.id')
            ->paginate(10);
        }
        $data['nilai'] = $nilai;
        return view('nilai.lihat', $data, compact('page', 'next', 'prev','numbering'));
    }

    public function TambahNilai(Request $request){
        $siswa = DB::table('users')
        ->select('users.name as nama_siswa', 'users.id as nomor_urut', 'model_has_roles.role_id')
        ->leftjoin('model_has_roles', 'users.id', 'model_has_roles.model_id')
        ->where('model_has_roles.role_id', 3)
        ->get();
        $matpel = DB::table('matpel')->get();
        return view('nilai.tambah', compact('siswa', 'matpel'));
    }

    public function EditNilai($id){
        $siswa = DB::table('users')
        ->select('users.name as nama_siswa', 'users.id as nomor_urut', 'model_has_roles.role_id')
        ->leftjoin('model_has_roles', 'users.id', 'model_has_roles.model_id')
        ->where('model_has_roles.role_id', 3)
        ->get();
        $matpel = DB::table('matpel')->get();
        $nilai = DB::table('nilai')->where('id', $id)->first();
        $data['nilai'] = $nilai;
        return view('nilai.edit', $data, compact('siswa', 'matpel'));
    }

    public function InsertNilai(Request $request){
        $id_siswa = $request->siswa;
        $id_matpel = $request->matpel;
        $nilai = $request->nilai;
        $nama_inputer = Auth::user()->name;
        $created_at = Carbon::now();

        DB::table('nilai')->insert([
            'id_siswa' => $id_siswa,
            'id_matpel' => $id_matpel,
            'nilai' => $nilai,
            'nama_inputer' => $nama_inputer,
            'created_at' => $created_at,
        ]);

        return redirect()->route('view_nilai')->with('success', 'Berhasil');
    }

    public function UpdateNilai(Request $request){
        $id = $request->id;
        $id_siswa = $request->siswa;
        $id_matpel = $request->matpel;
        $nilai = $request->nilai;
        $nama_inputer = Auth::user()->name;
        $updated_at = Carbon::now();

        DB::table('nilai')->where('id', $id)->update([
            'id_siswa' => $id_siswa,
            'id_matpel' => $id_matpel,
            'nilai' => $nilai,
            'nama_inputer' => $nama_inputer,
            'updated_at' => $updated_at,
        ]);

        return redirect()->route('view_nilai')->with('success', 'Berhasil');
    }

    public function DeleteNilai($id) {
        DB::table('nilai')->where('id', $id)->delete();
        return redirect()->route('view_nilai')->with('success', 'Berhasil');
    }
}
