<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;


class GuruController extends Controller
{
    public function ViewGuru (Request $request) {
        $page = 1;
        $next = $page+1;
        $prev = $page-1;
        $numbering = 0;
        if ($request->page) {
            $page = $request->page;
            $numbering = $page*10;
        }
        if ($request->page == 1) {
            $numbering = $page-1;
        }
        $guru = DB::table('users')
        ->select('users.id','users.name','users.email','roles.name as nama_posisi')
        ->leftjoin('model_has_roles as posisi', 'users.id','posisi.model_id')
        ->leftjoin('roles','posisi.role_id','roles.id')
        ->where('posisi.role_id', 2)
        ->paginate(10);
        $data['guru'] = $guru;
        return view('guru.view', $data, compact('page', 'next', 'prev','numbering'));
    }

    public function TambahGuru(Request $request){
        return view('guru.tambah');
    }

    public function EditGuru($id){
        $guru = DB::table('users')->where('id', $id)->first();
        $data['guru'] = $guru;
        return view('guru.edit', $data);
    }

    public function InsertGuru(Request $request){
        $name = $request->name;
        $email = $request->email;
        $created_at = Carbon::now();

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($request->password),
            'created_at' => $created_at,
        ]);

        $role = Role::findByName('guru');
        $user->assignRole($role);

        return redirect()->route('view_guru')->with('success', 'Berhasil');
    }

    public function UpdateGuru(Request $request){
        $id = $request->id;
        $name = $request->name;
        $email = $request->email;
        $update_at = Carbon::now();

        if($request->password != null) {
            User::where('id', $id)->update([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($request->password),
                'updated_at' => $update_at,
            ]);
        }
        else {
            User::where('id', $id)->update([
                'name' => $name,
                'email' => $email,
                'updated_at' => $update_at,
            ]);
        }

        return redirect()->route('view_guru')->with('success', 'Berhasil');
    }

    public function DeleteGuru($id){
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        DB::table('users')->where('id', $id)->delete();

        return redirect()->route('view_guru')->with('success', 'Berhasil');
    }
}
