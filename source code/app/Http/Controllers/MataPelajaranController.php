<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class MataPelajaranController extends Controller
{
    public function ViewMatpel(Request $request) {
        $page = 1;
        $next = $page+1;
        $prev = $page-1;
        $numbering = 0;
        if ($request->page) {
            $page = $request->page;
            $numbering = $page*10;
        }
        if ($request->page == 1) {
            $numbering = $page-1;
        }

        $matpel = DB::table('matpel')->paginate(10);
        $data['matpel'] = $matpel;
        return view('matpel.view', $data, compact('page', 'next', 'prev','numbering'));
    }

    public function TambahMatpel(Request $request){
        return view('matpel.tambah');
    }

    public function EditMatpel($id){
        $matpel = DB::table('matpel')->where('id', $id)->first();
        $data['matpel'] = $matpel;
        return view('matpel.edit', $data);
    }

    public function InsertMatpel(Request $request){
        $nama_matpel = $request->nama_matpel;
        $created_at = Carbon::now();

        DB::table('matpel')->insert([
            'nama_matpel' => $nama_matpel,
            'created_at' => $created_at,
        ]);

        return redirect()->route('view_matpel')->with('success', 'Berhasil');
    }

    public function UpdateMatpel(Request $request){
        $id = $request->id;
        $nama_matpel = $request->nama_matpel;
        $updated_at = Carbon::now();

        DB::table('matpel')->where('id', $id)->update([
            'nama_matpel' => $nama_matpel,
            'updated_at' => $updated_at,
        ]);

        return redirect()->route('view_matpel')->with('success', 'Berhasil');
    }

    public function DeleteMatpel($id){
        DB::table('matpel')->where('id', $id)->delete();

        return redirect()->route('view_matpel')->with('success', 'Berhasil');
    }


}
