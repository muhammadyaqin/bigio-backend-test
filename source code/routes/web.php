<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => ['permission:super admin']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('dashboard/guru', [App\Http\Controllers\GuruController::class, 'ViewGuru'])->name('view_guru');
    Route::get('dashboard/guru/tambah', [App\Http\Controllers\GuruController::class, 'TambahGuru'])->name('tambah_guru');
    Route::get('dashboard/guru/{id}/edit', [App\Http\Controllers\GuruController::class, 'EditGuru'])->name('edit_guru');
    Route::get('dashboard/guru/{id}/delete', [App\Http\Controllers\GuruController::class, 'DeleteGuru'])->name('delete_guru');
    Route::post('dashboard/guru/insert', [App\Http\Controllers\GuruController::class, 'InsertGuru'])->name('insert_guru');
    Route::post('dashboard/guru/update', [App\Http\Controllers\GuruController::class, 'UpdateGuru'])->name('update_guru');

    Route::get('dashboard/siswa', [App\Http\Controllers\SiswaController::class, 'ViewSiswa'])->name('view_siswa');
    Route::get('dashboard/siswa/tambah', [App\Http\Controllers\SiswaController::class, 'TambahSiswa'])->name('tambah_siswa');
    Route::get('dashboard/siswa/{id}/edit', [App\Http\Controllers\SiswaController::class, 'EditSiswa'])->name('edit_siswa');
    Route::get('dashboard/siswa/{id}/delete', [App\Http\Controllers\SiswaController::class, 'DeleteSiswa'])->name('delete_siswa');
    Route::post('dashboard/siswa/insert', [App\Http\Controllers\SiswaController::class, 'InsertSiswa'])->name('insert_siswa');
    Route::post('dashboard/siswa/update', [App\Http\Controllers\SiswaController::class, 'UpdateSiswa'])->name('update_siswa');

    Route::get('dashboard/matpel', [App\Http\Controllers\MataPelajaranController::class, 'ViewMatpel'])->name('view_matpel');
    Route::get('dashboard/matpel/tambah', [App\Http\Controllers\MataPelajaranController::class, 'TambahMatpel'])->name('tambah_matpel');
    Route::get('dashboard/matpel/{id}/edit', [App\Http\Controllers\MataPelajaranController::class, 'EditMatpel'])->name('edit_matpel');
    Route::get('dashboard/matpel/{id}/delete', [App\Http\Controllers\MataPelajaranController::class, 'DeleteMatpel'])->name('delete_matpel');
    Route::post('dashboard/matpel/insert', [App\Http\Controllers\MataPelajaranController::class, 'InsertMatpel'])->name('insert_matpel');
    Route::post('dashboard/matpel/update', [App\Http\Controllers\MataPelajaranController::class, 'UpdateMatpel'])->name('update_matpel');

    Route::get('dashboard/nilai', [App\Http\Controllers\DaftarNilaiController::class, 'ViewNilai'])->name('view_nilai');
    Route::get('dashboard/nilai/tambah', [App\Http\Controllers\DaftarNilaiController::class, 'TambahNilai'])->name('tambah_nilai');
    Route::get('dashboard/nilai/{id}/edit', [App\Http\Controllers\DaftarNilaiController::class, 'EditNilai'])->name('edit_nilai');
    Route::get('dashboard/nilai/{id}/delete', [App\Http\Controllers\DaftarNilaiController::class, 'DeleteNilai'])->name('delete_nilai');
    Route::post('dashboard/nilai/insert', [App\Http\Controllers\DaftarNilaiController::class, 'InsertNilai'])->name('insert_nilai');
    Route::post('dashboard/nilai/update', [App\Http\Controllers\DaftarNilaiController::class, 'UpdateNilai'])->name('update_nilai');

    Route::get('lihat-nilai', [App\Http\Controllers\DaftarNilaiController::class, 'LihatNilai'])->name('lihat_nilai');
});

Route::group(['middleware' => ['permission:input score']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('dashboard/nilai', [App\Http\Controllers\DaftarNilaiController::class, 'ViewNilai'])->name('view_nilai');
    Route::get('dashboard/nilai/tambah', [App\Http\Controllers\DaftarNilaiController::class, 'TambahNilai'])->name('tambah_nilai');
    Route::get('dashboard/nilai/{id}/edit', [App\Http\Controllers\DaftarNilaiController::class, 'EditNilai'])->name('edit_nilai');
    Route::get('dashboard/nilai/{id}/delete', [App\Http\Controllers\DaftarNilaiController::class, 'DeleteNilai'])->name('delete_nilai');
    Route::post('dashboard/nilai/insert', [App\Http\Controllers\DaftarNilaiController::class, 'InsertNilai'])->name('insert_nilai');
    Route::post('dashboard/nilai/update', [App\Http\Controllers\DaftarNilaiController::class, 'UpdateNilai'])->name('update_nilai');
});

Route::group(['middleware' => ['permission:view score']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('lihat-nilai', [App\Http\Controllers\DaftarNilaiController::class, 'LihatNilai'])->name('lihat_nilai');
});
