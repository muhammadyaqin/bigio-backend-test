@extends('adminlte::page')
@section('title', 'Edit Daftar Nilai')
@section('content_header')
    <h1>Edit Daftar Nilai</h1>
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Daftar Nilai</h3>
    </div>
    <div class="card-body">
        <form action="{{route('update_nilai')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Siswa</label>
                    <select class="form-control select2" name="siswa" id="siswa">
                        @foreach ($siswa as $nama)
                        <option {{ $nilai->id_siswa == $nama->nomor_urut ? 'selected':'' }} value="{{$nama->nomor_urut}}">{{$nama->nama_siswa}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" value="{{$nilai->id}}" name="id" id="id">
                </div>
                <div class="form-group">
                    <label for="matpel">Nama Mata Pelajaran</label>
                    <select class="form-control" name="matpel" id="matpel">
                        @foreach ($matpel as $pelajaran)
                        <option {{$nilai->id_matpel == $pelajaran->id ? 'selected':''}} value="{{$pelajaran->id}}">{{$pelajaran->nama_matpel}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nilai">Nilai</label>
                    <input type="number" value="{{$nilai->nilai}}" class="form-control" name="nilai" id="nilai" placeholder="Masukkan Nilai Siswa">
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
