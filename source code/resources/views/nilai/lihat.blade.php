@extends('adminlte::page')
@section('title', 'Daftar Nilai')
@section('content_header')
<h1>Daftar Nilai</h1>
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Daftar Nilai</h3>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                <th style="width: 10px">#</th>
                <th>Nama Siswa</th>
                <th>Nama Mata Pelajaran</th>
                <th>Nilai</th>
                </tr>
            </thead>
            <tbody>
                @foreach($nilai as $index => $list)
                <tr>
                <td>{{$numbering+$index+1}}</td>
                <td>{{$list->nama_siswa}}</td>
                <td>{{$list->nama_matpel}}</td>
                <td>{{$list->nilai}}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
    </div>
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            {{ $nilai->appends(request()->except('page'))->links() }}
        </ul>
    </div>
</div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
