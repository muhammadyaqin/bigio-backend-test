@extends('adminlte::page')
@section('title', 'Tambah Daftar Nilai')
@section('content_header')
    <h1>Tambah Daftar Nilai</h1>
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Tambah Daftar Nilai</h3>
    </div>
    <div class="card-body">
        <form action="{{route('insert_nilai')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Siswa</label>
                    <select class="form-control select2" name="siswa" id="siswa">
                        <option selected>Pilih Nama Siswa</option>
                        @foreach ($siswa as $nama)
                        <option value="{{$nama->nomor_urut}}">{{$nama->nama_siswa}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="matpel">Nama Mata Pelajaran</label>
                    <select class="form-control" name="matpel" id="matpel">
                        <option selected>Pilih Mata Pelajaran</option>
                        @foreach ($matpel as $pelajaran)
                        <option value="{{$pelajaran->id}}">{{$pelajaran->nama_matpel}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nilai">Nilai</label>
                    <input type="number" class="form-control" name="nilai" id="nilai" placeholder="Masukkan Nilai Siswa">
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
