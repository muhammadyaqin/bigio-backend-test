@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Welcome</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <h4>Selamat Datang ! {{Auth::user()->name}}</h4>
        </div>
        <!-- /.card-body -->
    </div>
      <!-- /.card -->
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
