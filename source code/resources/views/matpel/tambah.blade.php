@extends('adminlte::page')
@section('title', 'Tambah Daftar Mata Pelajaran')
@section('content_header')
    <h1>Tambah Daftar Mata Pelajaran</h1>
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Tambah Daftar Mata Pelajaran</h3>
    </div>
    <div class="card-body">
        <form action="{{route('insert_matpel')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Mata Pelajaran</label>
                    <input type="text" class="form-control" name="nama_matpel" id="nama_matpel" placeholder="Masukkan Nama Mata Pelajaran">
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
