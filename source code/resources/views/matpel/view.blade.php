@extends('adminlte::page')
@section('title', 'Daftar Mata Pelajaran')
@section('content_header')
<h1>Daftar Mata Pelajaran</h1>
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Daftar Mata Pelajaran</h3>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                <th style="width: 10px">#</th>
                <th>Nama Mata Pelajaran</th>
                <th>Label</th>
                </tr>
            </thead>
            <tbody>
                @foreach($matpel as $index => $list)
                <tr>
                <td>{{$numbering+$index+1}}</td>
                <td>{{$list->nama_matpel}}</td>
                <td>
                    <a href="{{url('dashboard/matpel/'.$list->id.'/edit')}}" class="badge bg-warning"><i class="fas fa-pen"></i></a>
                    <a href="{{url('dashboard/matpel/'.$list->id.'/delete')}}" class="badge bg-danger"><i class="fas fa-trash"></i></a>
                </td>
                </tr>
                @endforeach
            </tbody>
            </table>
    </div>
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            {{ $matpel->appends(request()->except('page'))->links() }}
        </ul>
    </div>
</div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
