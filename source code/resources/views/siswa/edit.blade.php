@extends('adminlte::page')
@section('title', 'Edit Daftar Siswa')
@section('content_header')
    <h1>Edit Daftar Siswa</h1>
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Daftar Siswa</h3>
    </div>
    <div class="card-body">
        <form action="{{route('update_siswa')}}" method="post">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" value="{{$siswa->name}}" class="form-control" name="name" id="name" placeholder="Masukkan Nama Lengkap">
                    <input type="hidden" value="{{$siswa->id}}" name="id" id="id">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" value="{{$siswa->email}}" class="form-control" name="email" id="email" placeholder="Masukkan Email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Kosongkan Saja Jika Tidak Ingin Update Password">
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop
